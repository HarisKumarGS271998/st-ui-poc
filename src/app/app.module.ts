import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import Amplify, {Interactions } from 'aws-amplify';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbChatModule, NbSpinnerModule, NbCardModule, NbButtonModule, NbSelectModule, NbToggleModule, NbFormFieldModule, NbInputModule, NbIconModule} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import { HomePageComponent } from './components/home-page/home-page.component';

Amplify.configure({
    Auth: {
      identityPoolId: 'us-east-1:8cd4bb92-4eae-46b1-b5f7-c43ff9fc95c9',
      region: 'us-east-1'
    },
    Interactions: {
      bots: {
        "nomad_dev": {
          "name": "nomad_dev",
          "alias": "$LATEST",
          "region": "us-east-1",
        },
      }
    }
  });

@NgModule({
  declarations: [
    AppComponent,
    ChatbotComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbChatModule,
    NbSpinnerModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    NbToggleModule,
    NbFormFieldModule,
    NbInputModule,
    NbIconModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
