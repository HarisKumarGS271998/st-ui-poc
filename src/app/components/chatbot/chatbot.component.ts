import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Interactions } from 'aws-amplify';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent implements OnInit {

  messages = [];
  loading = false;


  // Random ID to maintain session with server
  sessionId = Math.random().toString(36).slice(-5);

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.addBotMessage('Hi, Chris');
  }

  async handleUserMessage(event) {
    const text = event.message;
    this.addUserMessage(text);

    let response = await Interactions.send("nomad_dev", text.toString());

    if (response.message) {
      if(response.responseCard) {
        this.addBotMessage(response.message, response.responseCard.genericAttachments);
      } else {
        this.addBotMessage(response.message);
      }
      
    }
  }

  addUserMessage(text) {
    this.messages.push({
      text,
      sender: 'You',
      reply: true,
      date: new Date(),
    });
  }

  addBotMessage(text, genericAttachments?) {
    this.messages.push({
      text,
      sender: 'Nomad',
      avatar: '/assets/bot-avatar.jpg',
      date: new Date()
    });

    if(genericAttachments && genericAttachments.length !== 0) {
      const index = this.messages.length - 1;
      let checkExist = setInterval(() => {
        if (document.getElementById(`${index}-Nomad`)) {
           this.createAttachments(index, genericAttachments);
           clearInterval(checkExist);
        }
     }, 100); // check every 100ms   
    }
  }

  createAttachments(index, blogposts) {
    let count = 0;
    let elem:Element = document.getElementById(`${index}-Nomad`);
    let postContainer = document.createElement('div');
    postContainer.className = 'post-container'
      for(let post of blogposts) {
        count++;
        const postDiv = document.createElement('div');
        postDiv.className = 'post-div';
        const href = document.createElement('a');
        href.href = post.subTitle;
        href.target = '_blank';
        const cardContainer = document.createElement('div');
        cardContainer.className = 'post-card-container'
        const imageContainer = document.createElement('div');
        imageContainer.className = 'post-card-image-container'
        const linkContainer = document.createElement('div');
        linkContainer.className = 'link-container'
        const image = document.createElement('img');
        image.src = post.imageUrl;
        image.className = 'post-card-image'
        linkContainer.innerHTML = post.title;
        imageContainer.appendChild(image);
        cardContainer.appendChild(imageContainer);
        href.appendChild(linkContainer);
        cardContainer.appendChild(href);
        if(post.buttons[0].text === 'Book') {
          const button = document.createElement('button');
          button.className = 'response-elements response-buttons';
          button.innerHTML = 'Select';
          button.id = post.title;
          button.onclick = (event) => {
          let element = event.target as Element;
          this.handleUserMessage({message :element.id});
          }
          cardContainer.appendChild(button);
        }
        postDiv.appendChild(cardContainer);
        postContainer.appendChild(postDiv);
      }
    elem.insertAdjacentElement('afterend', postContainer);
  }
}
